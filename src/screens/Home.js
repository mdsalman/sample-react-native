import React from 'react';
import { Button, View } from 'react-native';

const data = [
    {
        profileName: 'salman',
        detail: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'
    },
    {
        profileName: 'rahul',
        detail: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'
    }
]

const Home = ({ navigation }) => {
    return (
        <View>
            {
                data.map((item) =>
                    <Button
                        title={item.profileName}
                        // onPress={() =>
                        //     navigation.navigate('Profile')
                        // }

                        onPress={() => navigation.navigate('Profile', {
                            text: 'Hello from screen 1'
                        })}
                    />
                )
            }
        </View>
    );
};

export default Home;